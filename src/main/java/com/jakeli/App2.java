package com.jakeli;

import net.sourceforge.tess4j.TesseractException;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import org.json.JSONArray;
import org.json.JSONException;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.json.JSONObject;


import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class App2 {
    //    private static String userName = "123";
//    private static String userPasswd = "456s";
    private static String userName = "ZHAORUNKANG";
    private static String userPasswd = "YX13702588082";
    private static final String loginUrl = "http://app.singlewindow.cn/cas/login?_local_login_flag=1&service=http://app.singlewindow.cn/cas/jump.jsp%3FtoUrl%3DaHR0cDovL2FwcC5zaW5nbGV3aW5kb3cuY24vY2FzL29hdXRoMi4wL2F1dGhvcml6ZT9jbGllbnRfaWQ9MTM2NyZyZXNwb25zZV90eXBlPWNvZGUmcmVkaXJlY3RfdXJpPWh0dHAlM0ElMkYlMkZ3d3cuc2luZ2xld2luZG93LmdkLmNuJTJGcG9ydGFsJTJGc3dPYXV0aExvZ2luLmFjdGlvbiUzRnNlcnZpY2VVcmwlM0Q=&localServerUrl=http://www.singlewindow.gd.cn&localDeliverParaUrl=/portal/getSWParams.action&isLocalLogin=1&localLoginLinkName=5pys5Zyw55m75b2V&localLoginUrl=L3BvcnRhbC9zd09hdXRoTG9naW4uYWN0aW9u&colorA1=FFFFFF&colorA2=60,60,61, 0.5&localRegistryUrl=aHR0cDovL3N3Y2FzLnNpbmdsZXdpbmRvdy5nZC5jbi9nZHN3L2V0cHNSZWdpc3Rlci9ldHBzUmVnaXN0ZXJOb0NhcmRQYWdlLmxlYWQ=";
    private static final String kHost = "http://www.singlewindow.gd.cn";

    private static final long kWaitTimeShort = 10;
    private static final long kWaitTimeLong = 20;//todo markmark 这里设置成20
    private static final long kWaitTimeImplicit = 5;
    private static final int kLoginTryTimes = 10;


    private static final long kTestWaitTime = 500;//todo markmark 这里设置成2000，停留页面的时间，测试用

    public static void main(String[] args) {
        setSystemConfig();

//        ChromeOptions options = new ChromeOptions();
//        options.setBinary("C:\\develop\\tools\\chromedriver\\chromedriver.exe");

        System.setProperty("webdriver.chrome.driver", "C:\\develop\\tools\\chromedriver\\chromedriver.exe");//chromedriver服务地址
        WebDriver driver = new ChromeDriver();
        driver.get(loginUrl);//打开指定的网站
        Set<Cookie> cookieSet = driver.manage().getCookies();
        String cookieToSet = "";
        for(Cookie c : cookieSet){
            cookieToSet = cookieToSet + c.getName() + "=" + c.getValue() + ";";
        }

        System.out.println("------------- login cookies ===============");
        System.out.println(cookieSet);
        driver.manage().timeouts().implicitlyWait(kWaitTimeImplicit, TimeUnit.SECONDS);


        String captcha = null;
        try {
            WebElement image = driver.findElement(By.id("vcode"));
            String vcodeSrc = image.getAttribute("src");
            BufferedImage saveImage = null;
            for(int j = 0; j < kLoginTryTimes; j++){
                WebElement username = driver.findElement(By.id("swy"));
                username.clear();
                username.sendKeys(userName);
                WebElement password = driver.findElement(By.id("swm2"));
                password.clear();
                password.sendKeys(userPasswd);
                URL imageUrl = new URL(vcodeSrc + "?r=" + Math.random());

                System.out.println("url = " + imageUrl.toString());
                URLConnection connection = imageUrl.openConnection();
                connection.setRequestProperty("Cookie", cookieToSet);
                saveImage = ImageIO.read(connection.getInputStream());

                /**
                 * 这是自己带着cookies请求回来的验证码图像。
                 */
                File outputFile = new File("downloadedimage.png");
                ImageIO.write(saveImage, "png", outputFile);


                /**
                 * 这是对验证码图像进行处理
                 */
                ImageUtil.binaryzation(saveImage);
                ImageIO.write(saveImage, "png", new File("image.png"));
                captcha = OCR.getTesseract().doOCR(saveImage).replaceAll(" ", "").trim();

                System.out.println("captcha = " + captcha);
                System.out.println(driver.manage().getCookies());

                WebElement captchaEdit = driver.findElement(By.id("verifyCode"));
                captchaEdit.clear();
                captchaEdit.sendKeys(captcha);

                // 左击实现（和元素的click类似）
                Actions action = new Actions(driver);
                WebElement test1item = driver.findElement(By.id("sub3"));
                action.click(test1item).perform();

                if(driver.getTitle().equals("个人消息")){
                    System.out.println("登陆成功！！！！！！！！！");
                    break;
                }
            }


            /**
             * 切换框架iframe，因为不切换findelement是找不到那个框架的元素的。
             */
            driver.switchTo().frame(0).switchTo().frame(0);

            /**
             * 把鼠标hover在“货物申报”按钮上面，因为selenium只能找到眼真正看到的按钮，这样后面findelemnt才找到
             */
            List<WebElement> navEleList = driver.findElement(By.className("asdiul")).findElements(By.className("yx"));
            for(WebElement e : navEleList){
                if(e.getText().equals("货物申报")){
                    Actions act = new Actions(driver);
                    act.moveToElement(e).perform();
                }
            }

            /**
             * 点击“货物申报”进去查询单子界面。
             */
            List<WebElement> seleList = driver.findElement(By.cssSelector(".aslia.yw6"))
                    .findElements(By.className("nrmc"));
            for(WebElement e : seleList){
                if(e.getText().equals("货物申报")){
                    e.click();
                }
            }

            /**
             * nav-label 点击“数据查询/统计”
             */
            /**
             * 这里会新建了一个页面，要切换window
             */
            Set<String> allWindows = driver.getWindowHandles();
            for(String s : allWindows){
                if(driver.switchTo().window(s).getTitle().equals("个人消息")){
                    continue;
                }

                //todo 在这里设置一个等待。
                new WebDriverWait(driver, kWaitTimeLong).until(new ExpectedCondition<Boolean>(){

                    @NullableDecl
                    @Override
                    public Boolean apply(@NullableDecl WebDriver webDriver) {
                        return webDriver.getTitle().equals("中国国际贸易单一窗口");
                    }
                });
            }

            /**
             * 点击左边菜单列表的“数据查询统计”按钮，会生成一些按钮给我点
             */
            List<WebElement> menuItemList = driver.findElements(By.className("nav-label"));
            for(WebElement e : menuItemList){
//                数据查询/统计
                if(e.getText().equals("数据查询/统计")){
                    e.click();
                    /**
                     *这里等个一两秒，等js生成html的东西，我要点击的按钮也是生成的。
                     */
                    WebElement accessItemUlTag = new WebDriverWait(driver, kWaitTimeLong).until(new ExpectedCondition<WebElement>(){
                        @NullableDecl
                        @Override
                        public WebElement apply(@NullableDecl WebDriver webDriver) {
                            return driver.findElement(By.xpath("//ul[@class='nav nav-third-level collapse in']"));
                        }
                    });
                    List<WebElement> accessItemList = accessItemUlTag.findElements(By.tagName("li"));
                    for (WebElement ae : accessItemList) {

                        if(ae.getText().equals("报关数据查询")){
                            ae.click();
                        }
                    }
                }
            }

            /**
             * 点击了“报关数据查询”之后，右边弹出新页面，点击“本周”radio按钮
             *
             */
            driver.switchTo().frame(1);
            WebElement queryRoot = driver.findElement(By.id("cus_basic_query"));
            WebElement querySelcRoot = queryRoot.findElement(By.className("col-sm-8"));
            List<WebElement> inputRoots = querySelcRoot.findElements(By.className("itswInput"));
            for(WebElement e : inputRoots){
                List<WebElement> iTagList = e.findElements(By.tagName("i"));
                for(WebElement ie : iTagList){
                    if(ie.getText().equals(" 本周")){
                        WebElement radioTag = e.findElement(By.id("operateDate2"));
                        radioTag.click();
                    }
                }
            }

            WebElement confirmBtn = driver.findElement(By.xpath("//button[@class='btn btn-sm btn-primary']"));
            confirmBtn.click();

            /**
             * 这里有个等待加载的地方。点击插叙那之后要等一下下面的单子列表加载
             */
            WebElement tbRootTag;
            WebElement queryResultRoot = driver.findElement(By.id("cus_declare_table_div"));
            List<WebElement> pageNumberTagList = queryResultRoot.findElements(By.xpath("//li[contains(@class, 'page-number')]"));

            /**
             * 筛选自定义属性data-index，遍历完之后就下一页
             */
            JSONObject jsonRoot = new JSONObject();
            JSONArray dataArr = new JSONArray();
            jsonRoot.put("data", dataArr);
            jsonRoot.put("crawler_start_time", System.currentTimeMillis());

            int curPage = 0;
            //遍历页面page
            while(curPage < pageNumberTagList.size()){
                //问题是这里还是拿回上一个driver
                 tbRootTag = waitAndGetCutomDataRoot(driver);

                List<WebElement> trList = tbRootTag.findElements(By.tagName("tr"));
                //遍历每一行的单子
                for(int i = 0; i < trList.size(); i++){
//                    if(i == 0) continue;
                    WebElement trTag = trList.get(i);
                    List<WebElement> tdList = trTag.findElements(By.tagName("td"));

                    WebElement customItemData = tdList.get(1);
                    WebElement aTag = customItemData.findElement(By.tagName("a"));
                    WebElement uTag = aTag.findElement(By.tagName("u"));
                    String uniId = uTag.getText();
                    System.out.println("-------------- uni id = --------------");
                    System.out.println(uniId);

                    /**
                     * 这里把东西滚到
                     */
                    driver.manage().timeouts().implicitlyWait(kWaitTimeImplicit, TimeUnit.SECONDS);
                    Actions actions = new Actions(driver);
                    actions.moveToElement(customItemData).build().perform();
                    customItemData.click();

                    /**
                     * 这里等待一会，才能swtich到点击单子后弹出的新frame。
                     */
                    driver.switchTo().defaultContent();
                    new WebDriverWait(driver, kWaitTimeLong).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(2));
                    WebElement customDataOpenedIndic = new WebDriverWait(driver, kWaitTimeLong).until(new ExpectedCondition<WebElement>() {
                        @NullableDecl
                        @Override
                        public WebElement apply(@NullableDecl WebDriver webDriver){
                            return webDriver.findElement(By.id("dec_head_form"));
                        }
                    });

                    //todo markmark 下面删掉
                    try {
                        Thread.sleep(kTestWaitTime);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    /**
                     * 上面已经等完单子加载了，可以开始爬取真正单子数据。
                     */

                    Set<Cookie> customCookieSet = driver.manage().getCookies();
                    cookieToSet = "";
                    for(Cookie c : customCookieSet){
                        cookieToSet = cookieToSet + c.getName() + "=" + c.getValue() + ";";
                    }

                    String urlStr = kHost + "/index/swProxy/decserver/sw/dec/merge/queryDecData";
                    String postData = "{\"cusCiqNo\":\"" + uniId + "\",\"cusIEFlag\":\"I\",\"operationType\":\"cusEdit\"}";
                    HttpURLConnection retConn = post(urlStr, postData, cookieToSet);
                    String sb = getResponse(retConn);


                    JSONObject rawObj = new JSONObject(sb.toString());
                    JSONObject dataObj = rawObj.getJSONObject("data").getJSONObject("preDecHeadVo");

                    //统一编号
                    String cusCiqNo = dataObj.getString("cusCiqNo");//uni_id  (_id)
                    //海关编号
                    String entryId = "";
                    if(dataObj.has("entryId")){
                        entryId = dataObj.getString("entryId");//customId
                    }

                    //单子状态  保存 审核  放行
                    String status = dataObj.getString("cusDecStatusName");//status

                    //发货人（委托人）公司编号
                    String consigneeCode = dataObj.getString("ownerCode");//consignorCode
                    //发货人（委托人）公司scc编号
                    String consigneeScc = dataObj.getString("ownerScc");//consigneeScc
                    //发货人（委托人）公司名字
                    String consigneeCname = dataObj.getString("ownerName");//consignorName

                    //代理公司编号
                    String agentCompanyCode = dataObj.getString("inputEtpsCode");//agentCompanyCode
                    //代理公司scc编号
                    String agentCompanyScc = dataObj.getString("agentScc");//agentCompanyScc
                    //代理公司名字
                    String agentCompanyName = dataObj.getString("agentName");//agentCompanyName

                    String updateTime = dataObj.getString("updateTime");//最近更新时间
                    //文件列表
                    String files = "";

                    String entryTypeName  = dataObj.getString("entryTypeName");//entryTypeName
                    String customMasterName = dataObj.getString("customMasterName");//申报海关地

                    JSONObject entryData = new JSONObject();
                    entryData.put("id", cusCiqNo);
                    entryData.put("customId", entryId);
                    entryData.put("status", status);
                    entryData.put("consignorCode", consigneeCode);
                    entryData.put("consignorScc", consigneeScc);
                    entryData.put("consignorName", consigneeCname);
                    entryData.put("agentCompanyCode", agentCompanyCode);
                    entryData.put("agentCompanyScc", agentCompanyScc);
                    entryData.put("agentCompanyName", agentCompanyName);
                    entryData.put("updateTime", updateTime);
                    entryData.put("entryTypeName", entryTypeName);
                    entryData.put("customMasterName", customMasterName);

//                    entryTypeName 是不是通关无纸化，从这里拿一下
                    /**
                     * 这里判断一下如果不是通关无纸化类型的单子，就没有“随单数据的”
                     */

                    if(entryTypeName.equals("通关无纸化")){
                        /**
                         * 点击“随单数据”完成，因为单子页面刚加载的时候这个按钮是不可点的，后面他用js又改成可点，所以要加个等待
                         */
                        WebElement attachmentBtn = driver.findElement(By.id("decDocBtn"));
                        new WebDriverWait(driver, kWaitTimeShort).until(ExpectedConditions.elementToBeClickable(By.id("decDocBtn")));
                        Actions decDocClickAct = new Actions(driver);
                        decDocClickAct.moveToElement(attachmentBtn).click().build().perform();

                        JSONObject fileRootObj = new JSONObject();
                        WebElement tbody = driver.findElement(By.id("decDocTable")).findElement(By.tagName("tbody"));
                        List<WebElement> fileTRTagList = tbody.findElements(By.tagName("tr"));
                        for(WebElement tre : fileTRTagList){
                            List<WebElement> fileTDList = tre.findElements(By.tagName("td"));
                            //看看请求要什么参数这里对应的td拿一下。
                            if(fileTDList.size() >= 4){
                                WebElement typeTD = fileTDList.get(0);
                                String typeStr = typeTD.getText();

                                WebElement fileNameTD = fileTDList.get(1);
                                String fileNameStr = fileNameTD.getText();

                                WebElement fileNoTD = fileTDList.get(2);
                                String fileNoStr = fileNoTD.getText();

                                WebElement downTD = fileTDList.get(3);
                                List<WebElement> downBtnList = downTD.findElements(By.tagName("button"));
//                                //todo markmark 这个是下载按钮，这里要拿一下onclick的属性值
                                String downClickVal = downBtnList.get(1).getAttribute("onclick");
                                Pattern p = Pattern.compile("\'.*\'");
                                Matcher m = p.matcher(downClickVal);
                                m.find();
                                downClickVal = m.group();
                                downClickVal = downClickVal.replaceAll("'", "");
                                String fileActualName = new String(downClickVal);
                                JSONObject fileDetailObj = new JSONObject();
                                fileDetailObj.put("type", typeStr);
                                fileDetailObj.put("file_name", fileNameStr);
                                fileDetailObj.put("file_no", fileNoStr);
                                fileRootObj.put(fileActualName, fileDetailObj);

                                String downFilePath = "attachments/" + uniId;
                                File fileDirectory = new File(downFilePath);
                                if(!fileDirectory.exists()){
                                    fileDirectory.mkdirs();
                                }

                                String downUrl = kHost + "/index/swProxy/decserver/sw/doc/cus/download/" + downClickVal;
                                String filePath = downFilePath + "/"  + downClickVal + ".pdf";

                                downloadFile(downUrl, filePath, cookieToSet);
                            }
                        }

                        entryData.put("files", fileRootObj);
                        dataArr.put(entryData);
                    }

                    driver.switchTo().defaultContent();
                    WebElement closeBtn = new WebDriverWait(driver, kWaitTimeShort).until(new ExpectedCondition<WebElement>() {
                        @NullableDecl
                        @Override
                        public WebElement apply(@NullableDecl WebDriver webDriver) {
                            List<WebElement> closeTagList = driver.findElements(By.xpath("//i[@class='fa fa-times-circle']"));
                            return closeTagList.get(1);
                        }
                    });
                    closeBtn.click();
//
//                    //todo markmark 下面删掉
                    try {
                        Thread.sleep(kTestWaitTime);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
//
//                    /**
//                     * 爬取完了之后就点击“叉叉”，回到之前的单子列表页面。
//                     */
                    driver.switchTo().defaultContent();
                    driver.switchTo().frame(1);

                    String customDownUrl = kHost + "/index/swProxy/decserver/entries/ftl/1/0/0/" + uniId + ".pdf";
                    String downFilePath = "attachments/" + uniId;
                    String relCustomDownPath = downFilePath + "/" +  "报关单_" + uniId + ".pdf";
                    downloadFile(customDownUrl, relCustomDownPath, cookieToSet);

                    String notiUrlPre = kHost + "/index/swProxy/decserver/sw/dec/common/queryCusDecRet?cusCiqNo=";
                    String notiUrlSur = "&limit=100&offset=0&0order=desc&stName=noticeDate";
                    String queryNotiUrl = notiUrlPre + uniId + notiUrlSur;

                    HttpURLConnection queryNotiConn = get(queryNotiUrl, cookieToSet);
                    String queryNotiRet = getResponse(queryNotiConn);
                    JSONObject queryNotiObj = new JSONObject(queryNotiRet);
                    JSONArray rowsArr = queryNotiObj.getJSONArray("rows");
                    String notiNo = "";
                    for(int j = 0; j < rowsArr.length(); j++){
                        JSONObject entry = rowsArr.getJSONObject(j);
                        if(entry.getString("note").equals(";报关单放行")){
                            notiNo = entry.getString("cusRetSeqNo");
                        }
                    }

                    if(!notiNo.isEmpty()){
                        String notiDownUrl = kHost + "/index/swProxy/decserver/ftlNotice/ftl/" + notiNo + ".pdf";
                        String notiDownFilePath = "attachments/" + uniId;
                        String relNotiDownPath = notiDownFilePath + "/" +  "通知书_" + notiNo + ".pdf";
                        downloadFile(notiDownUrl, relNotiDownPath, cookieToSet);

                    }

                }
                /**
                 * 点击下一页，这里每次都要重新找一下page标签，因为之前不招的话会出问题，最后以一个findelement就找不到了
                 */

                WebElement queryResultRootNew = driver.findElement(By.id("cus_declare_table_div"));
                List<WebElement> pageNumberTagListNew = queryResultRootNew.findElements(By.xpath("//li[contains(@class, 'page-number')]"));
                curPage++;
                if(curPage >= pageNumberTagListNew.size()){
                    break;
                }
                WebElement pageTag = pageNumberTagListNew.get(curPage);
                WebElement pageClickTag = pageTag.findElement(By.tagName("a"));
                pageClickTag.click();


                /**
                 * 点击下一页的时候，原来的tr元素会删掉，然后回异步下载实例化新的tr
                 * 所以这里隔一段时间判断是否原来的tr元素已经没了。才能开始findelement新的tr
                 */
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            File file = new File("somejsondata.json");
            FileWriter writer = new FileWriter(file);
            writer.write(jsonRoot.toString());
            writer.flush();
            writer.close();

        } catch (IOException | TesseractException | JSONException e) {
            e.printStackTrace();
        }finally {
            driver.quit();

        }


        try {
            Thread.sleep(40000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        try {
            Thread.sleep(50000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.quit();
    }


    /**
     * 这里用fiddle抓urlconnection的包一定要设置下面
     */
    private static void setSystemConfig(){
        System.setProperty("http.proxyHost", "localhost");
        System.setProperty("http.proxyPort", "8888");
        System.setProperty("https.proxyHost", "localhost");
        System.setProperty("https.proxyPort", "8888");
    }

    private static WebElement waitAndGetCutomDataRoot(WebDriver driver){
//        WebElement tbRootTag =
            return new WebDriverWait(driver, kWaitTimeLong).until(new ExpectedCondition<WebElement>() {
                @NullableDecl
                @Override
                public WebElement apply(@NullableDecl WebDriver webDriver) {
                    WebElement tableRoot = webDriver.findElement(By.id("declareTable"));
                    WebElement tbRoot = tableRoot.findElement(By.tagName("tbody"));
                    List<WebElement> trList = tbRoot.findElements(By.tagName("tr"));
                    if(trList.size() >= 1){
                        if(trList.get(0).findElements(By.tagName("td")).size() >= 1){
                            return tbRoot;
                        }
                    }
                    return null;
                }
        });
    }

    private static void downloadFile(String url, String filePath, String cookieToSet){
        try{
            URL downFileUrl = new URL(url);
            HttpURLConnection downConn = (HttpURLConnection)downFileUrl.openConnection();
            downConn.setDoOutput(true);
            downConn.setRequestProperty("Cookie", cookieToSet);
            InputStream downInputStream = downConn.getInputStream();

            FileOutputStream downOutputStream = new FileOutputStream(filePath);

            int byteRead = -1;
            byte[] buffer = new byte[1024];
            while((byteRead = downInputStream.read(buffer)) != -1){
                downOutputStream.write(buffer, 0, byteRead);
            }

            downOutputStream.close();
            downInputStream.close();

        }catch(IOException e){

        }

    }

    private static HttpURLConnection post(String urlStr, String postData, String cookieToSet){
        try{
            URL urlPost = new URL(urlStr);
            HttpURLConnection connection = (HttpURLConnection)urlPost.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-type", "application/json;charset=UTF-8");
            connection.setRequestProperty("Cookie", cookieToSet);

            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(postData);
            writer.flush();
            writer.close();

            return connection;
        }catch (IOException e){

        }

        return null;
    }

    private static HttpURLConnection get(String urlStr, String cookieToSet){
        try{
            URL urlPost = new URL(urlStr);
            HttpURLConnection connection = (HttpURLConnection)urlPost.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-type", "application/json;charset=UTF-8");
            connection.setRequestProperty("Cookie", cookieToSet);

            return connection;
        }catch (IOException e){

        }
        return null;
    }

    private static String getResponse(HttpURLConnection connection){
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            StringBuilder sb = new StringBuilder();
            while((line = reader.readLine()) != null){
                sb.append(line);
            }

            reader.close();

            return sb.toString();
        }catch(IOException e){

        }

        return null;

    }


    /**
     *  下面代码是截屏代码，之前用来截取验证码用的，现在用cookie请求可以了就废弃了。以后删掉。
     */
    private void screenShotCode(){
//            /****
//             * 这里是截屏得到的验证码，没什么用了，后面删掉。
//             */
//            File outputFile2 = new File("screenshot.png");
//            File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
//            org.openqa.selenium.Point point = image.getLocation();
//            //    // Get entire page screenshot
//            BufferedImage fullImg = ImageIO.read(screenshot);
//            // Get the location of element on the page
//            // Get width and height of the element
//            int eleWidth = image.getSize().getWidth();
//            int eleHeight = image.getSize().getHeight();
//            // Crop the entire page screenshot to get only element screenshot
//            BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth, eleHeight);
//            ImageIO.write(eleScreenshot, "png", screenshot);
//            FileUtils.copyFile(screenshot, outputFile2);

    }

}
